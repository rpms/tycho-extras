# The location of the xmvn dir into which we need to install the xmvn plugin
%global xmvn_libdir %(realpath $(dirname $(readlink -f $(which xmvn)))/../lib)

Name:           tycho-extras
Version:        1.4.0
Release:        1%{?dist}
Summary:        Additional plugins for Tycho

License:        EPL-1.0
URL:            http://eclipse.org/tycho/
Source0:        http://git.eclipse.org/c/tycho/org.eclipse.tycho.extras.git/snapshot/org.eclipse.tycho.extras-tycho-extras-%{version}.tar.xz
Patch0:         %{name}-fix-build.patch
Patch1:         %{name}-use-custom-resolver.patch
#https://git.eclipse.org/r/#/c/75453/
Patch2:         fix-xmvn-pomless-builddep.patch

BuildArch: noarch

ExclusiveArch:  x86_64

BuildRequires:  maven-local
BuildRequires:  mvn(io.takari.polyglot:polyglot-common)
BuildRequires:  mvn(org.apache.commons:commons-lang3)
BuildRequires:  mvn(org.apache.maven:maven-archiver)
BuildRequires:  mvn(org.apache.maven:maven-core)
BuildRequires:  mvn(org.apache.maven:maven-model)
BuildRequires:  mvn(org.apache.maven:maven-model-builder)
BuildRequires:  mvn(org.apache.maven:maven-plugin-api)
BuildRequires:  mvn(org.apache.maven.plugins:maven-plugin-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-source-plugin)
BuildRequires:  mvn(org.apache.maven.plugin-tools:maven-plugin-annotations)
BuildRequires:  mvn(org.codehaus.plexus:plexus-component-metadata)
BuildRequires:  mvn(org.eclipse.sisu:org.eclipse.sisu.plexus)
BuildRequires:  mvn(org.eclipse.tycho:org.eclipse.tycho.core.shared)
BuildRequires:  mvn(org.eclipse.tycho:org.eclipse.tycho.p2.resolver.shared)
BuildRequires:  mvn(org.eclipse.tycho:sisu-equinox-launching)
BuildRequires:  mvn(org.eclipse.tycho:tycho-artifactcomparator)
BuildRequires:  mvn(org.eclipse.tycho:tycho-core)
BuildRequires:  mvn(org.eclipse.tycho:tycho-p2-facade)
BuildRequires:  mvn(org.eclipse.tycho:tycho-packaging-plugin)
BuildRequires:  mvn(org.fedoraproject.p2:org.fedoraproject.p2)

%description
A small set of plugins that work with Tycho to provide additional functionality
when building projects of an OSGi nature.


%package javadoc
Summary:        Java docs for %{name}

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n org.eclipse.tycho.extras-tycho-extras-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1

# Plugins not needed for RPM builds
%pom_remove_plugin :maven-site-plugin

# maven-properties-plugin is only needed for tests
%pom_remove_plugin org.eclipse.m2e:lifecycle-mapping
%pom_remove_plugin org.sonatype.plugins:maven-properties-plugin tycho-p2-extras-plugin
# remove org.apache.maven:apache-maven zip
%pom_remove_dep org.apache.maven:apache-maven tycho-p2-extras-plugin
%pom_add_dep org.fedoraproject.p2:org.fedoraproject.p2 tycho-eclipserun-plugin/pom.xml

# Tycho plug-ins not needed for RPM builds of Eclipse plug-ins
%pom_disable_module tycho-buildtimestamp-jgit
%pom_disable_module tycho-sourceref-jgit

%mvn_alias :{*} org.eclipse.tycho:@1

%build
# To run tests, we need :
# maven-properties-plugin (unclear licensing)
%mvn_build -f

%install
%mvn_install

# Install extension JAR with deps into XMvn ext directory
install -d -m 755 %{buildroot}%{xmvn_libdir}/ext/
ln -s %{_javadir}/%{name}/tycho-pomless.jar %{buildroot}%{xmvn_libdir}/ext/
ln -s %{_javadir}/tesla-polyglot/polyglot-common.jar %{buildroot}%{xmvn_libdir}/ext/

%files -f .mfiles
%{xmvn_libdir}/ext/*

%files javadoc -f .mfiles-javadoc

%changelog
* Thu May 30 2019 Mat Booth <mat.booth@redhat.com> - 1.4.0-1
- Update to latest upstream release

* Thu May 23 2019 Mat Booth <mat.booth@redhat.com> - 1.3.0-4
- Don't build plug-ins we can't use during RPM builds

* Tue Mar 19 2019 Mat Booth <mat.booth@redhat.com> - 1.3.0-3
- Restrict to same architectures as Eclipse itself

* Tue Mar 12 2019 Mat Booth <mat.booth@redhat.com> - 1.3.0-2
- Make installation more portable

* Tue Feb 19 2019 Mat Booth <mat.booth@redhat.com> - 1.3.0-1
- Update to latest upstream release

* Sun Feb 03 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Fri Aug 17 2018 Mat Booth <mat.booth@redhat.com> - 1.2.0-3
- License correction

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 05 2018 Mat Booth <mat.booth@redhat.com> - 1.2.0-1
- Update to latest release

* Wed May 02 2018 Mat Booth <mat.booth@redhat.com> - 1.1.0-1
- Update to latest upstream release

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Apr 20 2017 Mat Booth <mat.booth@redhat.com> - 1.0.0-1
- Update to 1.0.0

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.26.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Wed Nov 02 2016 Mat Booth <mat.booth@redhat.com> - 0.26.0-1
- Update to latest upstream release

* Mon Jul 4 2016 Alexander Kurtakov <akurtako@redhat.com> 0.25.0-4
- Add patch to fix NPE in xmvn builddep.

* Wed Jun 15 2016 Mikolaj Izdebski <mizdebsk@redhat.com> - 0.25.0-3
- Add missing build-requires

* Thu Apr 28 2016 Mikolaj Izdebski <mizdebsk@redhat.com> - 0.25.0-2
- Obsolete tycho-pomless

* Thu Apr 21 2016 Mat Booth <mat.booth@redhat.com> - 0.25.0-1
- Update to latest upstream release

* Thu Apr 14 2016 Mat Booth <mat.booth@redhat.com> - 0.23.0-4
- Fix build against new maven-archiver, which removed some deprecated methods
  that tycho was using

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.23.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.23.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Tue Jun 9 2015 Alexander Kurtakov <akurtako@redhat.com> 0.23.0-1
- Update to upstream 0.23.

* Fri Dec  5 2014 Mikolaj Izdebski <mizdebsk@redhat.com> - 0.22.0-2
- Port to latest fedoraproject-p2

* Mon Dec 01 2014 Mat Booth <mat.booth@redhat.com> - 0.22.0-1
- Update to tagged release
- Fix directory ownership problem

* Tue Nov 25 2014 Roland Grunberg <rgrunber@redhat.com> - 0.22.0-0.1.gitef068a
- Update to 0.22.0 pre-release.

* Wed Sep 03 2014 Roland Grunberg <rgrunber@redhat.com> - 0.21.0-3
- Use fedoraproject-p2 to do OSGi bundle discovery.

* Thu Aug 21 2014 Roland Grunberg <rgrunber@redhat.com> - 0.21.0-2
- Integrate fedoraproject-p2 functionality.

* Fri Jul 25 2014 Roland Grunberg <rgrunber@redhat.com> - 0.21.0-1
- Update to 0.21.0 Release.

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.20.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Mar 25 2014 Roland Grunberg <rgrunber@redhat.com> - 0.20.0-1
- Update to 0.20.0 Release.

* Tue Mar 11 2014 Michael Simacek <msimacek@redhat.com> - 0.19.0-3
- Use mvn_build and mvn_install.
- Drop manual requires.

* Thu Feb 27 2014 Roland Grunberg <rgrunber@redhat.com> - 0.19.0-2
- Change R:java to R:java-headless (Bug 1068575).

* Fri Oct 25 2013 Roland Grunberg <rgrunber@redhat.com> - 0.19.0-1
- Update to 0.19.0 Release.

* Mon Jul 29 2013 Roland Grunberg <rgrunber@redhat.com> 0.18.1-1
- Update to 0.18.1 Release.

* Thu May 30 2013 Roland Grunberg <rgrunber@redhat.com> 0.18.0-1
- Update to 0.18.0 Release.

* Tue May 7 2013 Roland Grunberg <rgrunber@redhat.com> 0.17.0-2
- tycho-eclipserun-plugin should use the system local p2 repo.

* Tue Apr 2 2013 Roland Grunberg <rgrunber@redhat.com> 0.17.0-1
- Update to 0.17.0 Release.

* Mon Feb 25 2013 Roland Grunberg <rgrunber@redhat.com> 0.17.0-0.1.git0a9370
- Update to latest 0.17.0-SNAPSHOT.

* Thu Feb 21 2013 Roland Grunberg <rgrunber@redhat.com> - 0.16.0-5
- Fix PlexusConfiguration class issues identically across branches.

* Wed Feb 20 2013 Roland Grunberg <rgrunber@redhat.com> - 0.16.0-4
- Fix build issues relating to PlexusConfiguration.

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Dec 13 2012 Roland Grunberg <rgrunber@redhat.com> 0.16.0-3
- Fix upstream Bug 393686.

* Fri Oct 19 2012 Roland Grunberg <rgrunber@redhat.com> 0.16.0-2
- Update to 0.16.0 Release.

* Mon Jul 30 2012 Roland Grunberg <rgrunber@redhat.com> 0.16.0-1.e58861
- Update to 0.16.0 SNAPSHOT.

* Fri Jul 27 2012 Roland Grunberg <rgrunber@redhat.com> 0.15.0-1
- Update to 0.15.0.

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.14.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Apr 16 2012 Roland Grunberg <rgrunber@redhat.com> - 0.14.0-1
- Initial packaging of tycho extras.
